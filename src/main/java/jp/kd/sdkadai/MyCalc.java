package jp.kd.sdkadai;

import java.math.BigInteger;

// newVMtest
public class MyCalc {
	
	private Integer num = 0;

	public Integer getNum() {
		// TODO Auto-generated method stub
		return this.num;
	}
	
	public void setNum(Integer num){
		this.num = num;
	}

	public int ex1(int multiplicand){
		int ret = multiplicand * num;
		return (ret > 0) ? ret : -ret;
	}

	public Boolean ex2() {
		// TODO Auto-generated method stub
		
		int n = num; // speed up!
		
		// All number less than 1 return false
		// and 1 is not prime... false
		if(n<2){
			return false;
		}
		
		// loop i incring
		// if n%i == 0, n is not prime
		// if square of i becomes greater than n, n is prime
		for(int i=2; (i*i)<=n; i++){
			if(n%i==0){
				return false; // Go home
			}
		}
		
		return true; // prime! Congratulations
	}

	public String ex3(Integer num2) {
		BigInteger big = new BigInteger(num2.toString());
		big = big.pow(num2);
		return big.toString();
	}
	
}
